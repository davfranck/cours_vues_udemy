import Vue from 'vue'
import App from './App.vue'
import Home from './Home.vue'

// le composant est enregistré de façon globale et donc accessible à toutes les instances de Vue
Vue.component('app-servers', Home);

new Vue({
  el: '#app',
  render: h => h(App)
})
