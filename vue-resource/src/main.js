import Vue from 'vue'
import VueResource from 'vue-resource';
import App from './App.vue'

Vue.use(VueResource);

// valeurs par défaut pour l'url de base
Vue.http.options.root = 'https://vuejs-http-4cf05.firebaseio.com/';
// on peut définir d'autres propriétés globales, comme les headers.

// interception sur chaque requete et réponse
Vue.http.interceptors.push((request, next) => {
  console.log(request);
  next(response => {
    console.log(response);
  });
});

new Vue({
  el: '#app',
  render: h => h(App)
})
