new Vue({
  el: "#app",
  data: {
    game: {},
    showActions: false,
    history: []
  },
  methods: {
    newGame: function() {
      this.game = {
        playerHealth: 100,
        monsterHealth: 100
      };
      this.showActions = true;
      this.history = [];
      console.log('new game !');
    },
    attack: function (){
      var playerHit =  Math.floor((Math.random() * 10) + 1);
      var monsterHit =  Math.floor((Math.random() * 8) + 1);

      this.game.playerHealth -= monsterHit;
      this.addToLog('PLAYER HITS MONSTER FOR ' + playerHit, 'player-turn');

      this.game.monsterHealth -= playerHit;
      this.addToLog('MONSTER HITS PLAYER FOR ' + monsterHit, 'monster-turn');

      console.log('attack done');
    },
    specialAttack: function() {
        console.log('special attack');
    },
    heal: function(){
        console.log('heal');
        var playerHeal =  Math.floor((Math.random() * 10) + 1);
        this.game.playerHealth += playerHeal;
        this.addToLog('PLAYER HEALS FOR ' + playerHeal, 'player-turn');

        var monsterHit =  Math.floor((Math.random() * 8) + 1);
        this.game.playerHealth -= monsterHit;
        this.addToLog('MONSTER HITS PLAYER FOR ' + monsterHit, 'monster-turn');
    },
    addToLog: function(message, cssClass) {
      this.history.push('<span class="' + cssClass + '">' + message + '</span>');
      console.log('history updated');
    }
  }
});
