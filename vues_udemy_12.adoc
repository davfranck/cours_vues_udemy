# Advanced component usage

## Slots

_Les slots sont utiles quand on veut maitriser le Html dans le composant parent._

Cf projet 'advanced-component-usage'.

Balise `<slot></slot>` dans le template du composant enfant.

Dans le composant parent on met le contenu à l'intérieur de la balise du composant enfant :

----
<app-quote>
  <h1>{{quoteTitle}}</h1>
  <p>Paragraphe</p>
</app-quote>
----

Le style est appliqué via le composant enfant, le reste via le parent : `<slot name="title"></slot>`.

### named slots

On ajoute l'attribut 'name' au tag slot dans le composant enfant.

On ajoute au tag dans le parent l'attribut 'slot="nomDuSlot"'. Ex :

----
<app-quote>
  <h1 slot="title">{{quoteTitle}}</h1>
  <p slot="content">Paragraphe</p>
</app-quote>
----

### Slot default

Si sur un slot on ne met pas de nom, alors ce slot sera considéré comme slot par défaut.

### Données par défaut

Si on n'a pas de données dans un slot on peut mettre des données par défaut.
Ex : `<slot name="subtitle">The Subtitle</slot>`.

## Changer de composant dynamiquement

Déclaration : `<component :is="selectedComponent"></component>`

La propriété `selectedComponent` est retournée parmis les 'data' et contient le nom du composant, 'appServers' par exemple.
En changeant la valeur de cette propriété on peut donc changer dynamiquement le composant.

Le composant est détruit à chaque switch.
Pour éviter cela on utilise `keep-alive` :

----
<keep-alive>
  <component :is="...">
    <p>Default content</p>
  </component>
</keep-alive>
----

On peut alors utiliser des hooks pour réagir au switch : `deactivated` et `activated`.

# Ajout d'un évènement dom sur un composant

On ne peut pas écrire `<app-moncomposant @click="maMethode"></...>` car l'évènement click n'est pas attaché au dom.
Cependant on a une solution : `@click.native`
