# Evènement dom avec v-on

Cf vuejs_udemy_1.html :

Pour réagir à un évènement du dom on utilise v-on ou @ : `<input type="text" v-on:input="changeTitle">`.
Ici la méthode `changeTitle` de la vue est appelée.

Toutes les évènements du dom pour button sont autorisés.
On peut ajouter de paramètres perso avec l'évènement. Ex : `<button v-on:click="increase(2, $event)">Click Me</button>`

Cf vuejs_udemy_3.html :

On peut ajouter des modifiers aux listeners.
Cf : https://vuejs.org/v2/guide/events.html#Event-Modifiers

Ex : `<input type="text" v-on:keyup.enter="alertMe">`

# Data binding avec {{ }} et v-bind

Cf vuejs_udemy_2.html

Les {{ }} sont utilisés là où on aurait du texte.
Pas besoin de this.

Pour les attributs on utilise `v-bind`
A la place de `v-bind:href="maVariable"` on peut simplement écrire `:href="maVariable"`.

On utilise `v-once` pour évaluer une seule fois la variable.

# Interpréter du html avec v-html

Cf vuejs_udemy_2.html

Permet d'interpréter du html stocké dans une chaine.
Attention au cross scripting `<p v-html="completeLink"></p>`

# Data-binding bi directionnel

On utilise v-model qui permet d'activer le data-binding bidirectionnel entre une zone de saisie et une propriété de la vue.

# Computed properties

Cf vuejs_udemy_4.html

Les fonctions de type "computed" ne sont appelées que pour les variables qui sont modifiées dans le corps.
Vue crée automatiquement une propriété pour les données "computed".
En revanche, les fonctions dans la propriété "methods" sont toujours évaluées.

# Watch

Cf vuejs_udemy_4.html

Exécuté à chaque fois qu'une propriété change.

# Attributs class et style

Cf vuejs_udemy_5.html

Dans :class on peut mettre :

* une propriété qui contient la classe {cssClass: property}
* un objet qui contient les classes à lier et la condition associée
* un tableau, par exemple : :class=[color, background] (et mixer propriété et objet)
Aussi Le tableau peut contenir le nom des classes.

# v-if / v-show

Cf vuejs_udemy_6.html

* v-if : retire l'élément du dom si l'évaluation est négative.
* v-else : par rapport au précédent v-if

Maintenant il existe aussi v-else-if (https://vuejs.org/v2/api/#v-else-if)

v-show ne retire pas du dom, il ajoute `display:none`.

# v-for

Cf vuejs_udemy_7.html

Il est conseillé de toujours utilisé :key afin de ne pas modifier le contenu des références,
mais bien de réordonner les éléments quand le contenu change.

On peut utiliser la balise `<template>` quand on veut itérer sur plusieurs tags qui npartagent un parent avec d'autres tags que l'on ne veut pas prendre en compte dans l'itération.

# Vue.component

Cf vuejs_udemy_9.html et vuejs_udemy_10.html
