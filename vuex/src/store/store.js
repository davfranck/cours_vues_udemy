import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    counter: 0,
    value: 0
  },
  getters: {
    doubleCounter: state => {
      return state.counter * 2;
    },
    stringCounter: state =>  {
      return state.counter + ' clicks'  ;
    },
    value: state =>{
      return state.value;
    }
  },
  mutations: {
    increment: state =>  {
      state.counter++;
    },
    decrement: state => {
      state.counter--;
    },
    updateValue:  (state, payload) => {
      state.value = payload;
    }
  }
});
